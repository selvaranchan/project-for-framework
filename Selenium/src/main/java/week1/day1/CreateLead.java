package week1.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window ().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("INFOSYS");
		driver.findElementById("createLeadForm_firstName").sendKeys("ARUN");
		driver.findElementById("createLeadForm_lastName").sendKeys("M");
		WebElement dtsrc = driver.findElementById("createLeadForm_dataSourceId");
		Select drop = new Select(dtsrc);
		drop.selectByIndex(3);
		driver.findElementById("createleadform_marketcampaignid").sendKeys("catalogue generating");
		driver.findElementById("createleadform_firstnamelocal").sendKeys("arun");
		driver.findElementById("createleadform_lastnamelocal").sendKeys("M");
		driver.findElementById("createleadform_personaltitle").sendKeys("onlineapp");
		driver.findElementById("createleadform_generalproftitle").sendKeys("shopping");
		driver.findElementById("createleadform_departmentname").sendKeys("purchase");
		driver.findElementById("createleadform_annualrevenue").sendKeys("50000");
		driver.findElementById("cretateleadform_industryenumid").sendKeys("computerharware");
		driver.findElementById("createleadform_numberemployees").sendKeys("30");
		driver.findElementById("createleadform_ownershipenumid").sendKeys("own");
		driver.findElementById("createleadform_siccode").sendKeys("1");
		driver.findElementById("createleadform_ticketsymbol").sendKeys("s");
		driver.findElementById("createleadform_description").sendKeys("a to z");
		driver.findElementById("createleadform_importantnote").sendKeys("i am a hero ");
		driver.findElementById("createleadform_primaryphoneareacode").sendKeys("+91");
		driver.findElementById("createleadform_primaryphonenumber").sendKeys("1234567890");
		driver.findElementById("createleadform_primaryphoneextension").sendKeys("2536");
		driver.findElementById("createleadform_primaryphoneaskforname").sendKeys("arun M");
		driver.findElementById("createleadform_primaryphoneaskforname").sendKeys("arunm@gmail.com");
		driver.findElementById("createleadform_primarywebur1").sendKeys("http:/www.amazon.in/");
		driver.findElementById("createleadform_generaltoname").sendKeys("arun prabhu");
		driver.findElementById("createleadform_generalatname").sendKeys("arun");
		driver.findElementById("createleadform_generaladdress1").sendKeys("123 abc street");
		driver.findElementById("createleadform_generaladdress2").sendKeys("chennai");
		driver.findElementById("createleadform_stateprovincegeoid").sendKeys("TN");
		driver.findElementById("createleadform_generalpostalcode").sendKeys("123456");
		driver.findElementById("createleadform_generalcountrycode").sendKeys("INDIA");
		driver.findElementById("createleadform_generalpostalcodeext").sendKeys("7890");
		driver.findElementByName("submitbutton").click();
	}
}