package week5.day3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regexpattern {

	public static void main(String[] args) {
	   String actualDATA = "1234 5678 9012 3456";
	   String pat = "[0-9] \\s [0-9] \\s [0-9] \\s [0-9] ";
	   Pattern compile = Pattern.compile(pat);
	   Matcher matcher = compile.matcher(actualDATA);
	   System.out.println(matcher.matches());
	   
	   
	}

}
