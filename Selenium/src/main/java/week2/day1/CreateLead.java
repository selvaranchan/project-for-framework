package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateLead {

	public  void createlead() {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window ().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByLinkText("CreateLeadForm_companyName").sendKeys("INFOSYS");
		driver.findElementByLinkText("CreateLeadForm_firstname").sendKeys("ARUN");
		driver.findElementByLinkText("CreateLeadForm_lastname").sendKeys("M");
		driver.findElementByLinkText("createleadform_datasourceid").sendKeys("employee");
		driver.findElementByLinkText("createleadform_marketcampaignid").sendKeys("catalogue generating");
		driver.findElementByLinkText("createleadform_firstnamelocal").sendKeys("arun");
		driver.findElementByLinkText("createleadform_lastnamelocal").sendKeys("M");
		driver.findElementByLinkText("createleadform_personaltitle").sendKeys("onlineapp");
		driver.findElementByLinkText("createleadform_generalproftitle").sendKeys("shopping");
		driver.findElementByLinkText("createleadform_departmentname").sendKeys("purchase");
		driver.findElementByLinkText("createleadform_annualrevenue").sendKeys("50000");
		driver.findElementByLinkText("cretateleadform_industryenumid").sendKeys("computerharware");
		driver.findElementByLinkText("createleadform_numberemployees").sendKeys("30");
		driver.findElementByLinkText("createleadform_ownershipenumid").sendKeys("own");
		driver.findElementByLinkText("createleadform_siccode").sendKeys("1");
		driver.findElementByLinkText("createleadform_ticketsymbol").sendKeys("s");
		driver.findElementByLinkText("createleadform_description").sendKeys("a to z");
		driver.findElementByLinkText("createleadform_importantnote").sendKeys("i am a hero ");
		driver.findElementByLinkText("createleadform_primaryphoneareacode").sendKeys("+91");
		driver.findElementByLinkText("createleadform_primaryphonenumber").sendKeys("1234567890");
		driver.findElementByLinkText("createleadform_primaryphoneextension").sendKeys("2536");
		driver.findElementByLinkText("createleadform_primaryphoneaskforname").sendKeys("arun M");
		driver.findElementByLinkText("createleadform_primaryphoneaskforname").sendKeys("arunm@gmail.com");
		driver.findElementByLinkText("createleadform_primarywebur1").sendKeys("http:/www.amazon.in/");
		driver.findElementByLinkText("createleadform_generaltoname").sendKeys("arun prabhu");
		driver.findElementByLinkText("createleadform_generalatname").sendKeys("arun");
		driver.findElementByLinkText("createleadform_generaladdress1").sendKeys("123 abc street");
		driver.findElementByLinkText("createleadform_generaladdress2").sendKeys("chennai");
		driver.findElementByLinkText("createleadform_stateprovincegeoid").sendKeys("TN");
		driver.findElementByLinkText("createleadform_generalpostalcode").sendKeys("123456");
		driver.findElementByLinkText("createleadform_generalcountrycode").sendKeys("INDIA");
		driver.findElementByLinkText("createleadform_generalpostalcodeext").sendKeys("7890");
		driver.findElementByLinkText("submitbutton").click();
		
		
	}
};