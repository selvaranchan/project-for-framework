package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class Learnframes {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window ().maximize();
        driver.switchTo().frame("iframeResult");
        driver.findElementByXPath("//button[text()='Try it']").click();
        String text = driver.switchTo().alert().getText();
        System.out.println(text);
        driver.switchTo().alert().sendKeys("arun");
        Thread.sleep(5000);
        driver.switchTo().alert().accept();
        String text2 = driver.findElementById("demo").getText();
        System.out.println(text2);
        if (text2.contains("arun")) {
        	System.out.println("text matches");
			
		} else {
			System.out.println("text does not match");

		}
        
     
	}

}
